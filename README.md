# What Is DeepSage?
Yet another Natural Language Processing (NLP) library

# DeepSage Key Concepts

## Information Interpretations

One of key features of natural language is that often we have more than one interpretation of one message.
We also can find new interpretations after getting some addition messages.
So...

> Message interpretation can not be final

During all our life we get new information and re-understand things. 
To solve NLP tasks system should hold array of interpretations for every message.

## What Does "Understanding" Mean?

If interpretations is not stable thing. As we can get new information that will update our interpretation
of previous message, we can say that...

> Understanding is some kind of illusion with some level of confidence

## Deep Graph Data Model

To store information in computer readable way we use unique approach called "Deep Graph".

> Deep Graph is [Resource Description Framework](https://en.wikipedia.org/wiki/Resource_Description_Framework) Graph extended with predicate "Deep"
> that makes each vertex become Deep Graph by itself

## Rules vs Statistics

We use both. 

## Data corpus

We use open parallel texts in several languages and open projects like wikidata, wiktionary.
